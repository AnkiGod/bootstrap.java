package com.aplynk.bootstrap.blueprints;

import com.google.gson.JsonObject;
import io.elastic.api.Component;
import io.elastic.api.EventEmitter;
import io.elastic.api.Message;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by soumya on 02-05-2016.
 */
public abstract class BaseComponent extends Component{
    private EventEmitter eventEmitter;
    private Map<String, String> mockEnvvars = new HashMap<String, String>();
	private boolean testing = false;

    public BaseComponent(EventEmitter eventEmitter) {
        super(eventEmitter);
        this.eventEmitter = eventEmitter;
    }

    public void emitData(JsonObject data) {
        Message message = new Message.Builder().body(data).build();
        this.eventEmitter.emitData(message);
    }

    public void emitError(Exception e) {
        eventEmitter.emitException(e);
    }

    public void emitError(String message) {
        emitError(new RuntimeException(message));
    }

	public void emitRebound(String reason) {eventEmitter.emitRebound(reason);}

	public void emitSnapshot(JsonObject data) {eventEmitter.emitSnapshot(data);}

	public void emitToken(JsonObject jObj) {
		eventEmitter.emitUpdateKeys(jObj);
	}

    public void log(String message) {
        System.out.println("--- " + new Date()+ " --- | -----> " + message);
    }

	public String getEnv(String key) {
		if(!testing) {
			return System.getenv(key);
		} else {
			return mockEnvvars.get(key);
		}
	}

	public void addMockEnvVar(String key, String val) {
		testing = true;
		mockEnvvars.put(key, val);
	}
}
