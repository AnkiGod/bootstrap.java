package com.aplynk.bootstrap.models;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.aplynk.bootstrap.models.standard.OAuth;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Blaze on 10-05-2016.
 */
public class Config extends JsonModel {
	public String getDivision() {
		return division;
	}

	@SerializedName("division")
	private String division;

	public OAuth getOauth() {
		return oauth;
	}

	@SerializedName("oauth")
	private OAuth oauth;

	@Override
	public String toString() {
		return toJson().toString();
	}

	@Override
	public JsonObject toJson() {
		return getJson(this);
	}


}
