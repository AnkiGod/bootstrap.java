package com.aplynk.bootstrap.models.standard;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Soumya on 05-12-2016.
 */

public class ALineItem extends JsonModel {
    private String quantity;

    @SerializedName("list_price")
    private String listPrice;

    @SerializedName("tax_code")
    private String taxCode;

    @SerializedName("tax_percentage")
    private String taxPercentage;

    @SerializedName("discount_code")
    private String discountCode;

    @SerializedName("discount_percentage")
    private String discountPercentage;

    @SerializedName("sub_total")
    private String subTotal;

    @SerializedName("tax_total")
    private String taxTotal;

    private String total;

    @SerializedName("external_reference")
    private String externalReference;

    private AProduct product;

    public void checkValidity() {
        if(!isValid()) {
            throw new RuntimeException("Line Item invalid");
        }
        product.checkValidity();
    }

    private boolean isValid() {
        return quantity != null && quantity.length() > 0 && listPrice != null && listPrice.length() > 0;
    }

    public String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getListPrice() {
        return listPrice;
    }

    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(String taxTotal) {
        this.taxTotal = taxTotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public AProduct getProduct() {
        return product;
    }

    public void setProduct(AProduct product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return getJson(this);
    }
}
