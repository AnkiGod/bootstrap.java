package com.aplynk.bootstrap.triggers;

import com.aplynk.bootstrap.blueprints.BaseComponent;
import com.aplynk.bootstrap.models.Config;
import com.aplynk.bootstrap.network.RequestHelper;
import com.aplynk.bootstrap.network.apis.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.elastic.api.EventEmitter;
import io.elastic.api.ExecutionParameters;

import java.util.*;



/**
 * Created by Anantaram on 07-04-2017.
 */
public class GetItemDescription extends BaseComponent {

    public GetItemDescription(EventEmitter eventEmitter) {
        super(eventEmitter);
    }

    private String clientId;
    private String clientSecret;
    private Config config;
    private JsonObject input;
    private RequestHelper requestHelper;
    private WarehouseApi warehouseApi;
    private String itemName;
    private ItemsApi itemsApi;
    private String itemID;
    private Integer itemStock;
    private Double itemPrice;
    private JsonObject itemDetails;
    private JsonArray Items=new JsonArray();
    public int i;
    private JsonObject salesItemPrice;
    private JsonObject jsonObject;
    private String A;
    private String B;
    private String C;
    private String D;
    private String E;
    private String F;


    @Override
    public void execute(ExecutionParameters parameters) {
        log("Config :: " + parameters.getConfiguration());
        log("Snapshot :: " + parameters.getSnapshot());

        clientId=getEnv("client_id");
        clientSecret=getEnv("client_secret");
        config = new Gson().fromJson(parameters.getConfiguration(), Config.class);

        log("config Model : " + config);
        log("Client Id : " + clientId);
        log("Client Secret : " + clientSecret);

        requestHelper=new RequestHelper();
        String token=refreshToken();
        getItemDetails(token);

    }

    private void getItemDetails(String token) {
        warehouseApi = requestHelper.getApiBuilder(token).create(WarehouseApi.class);
        JsonObject warehousedesc = warehouseApi.getWarehouseByCode(config.getDivision(), "Code eq '5566555'");
        String warehouseId = warehousedesc.get("d").getAsJsonObject().get("results").getAsJsonArray().get(0).getAsJsonObject().get("ID").getAsString();
        itemsApi = requestHelper.getApiBuilder(token).create(ItemsApi.class);
        JsonObject itemDesc = itemsApi.getItemByWarehouseID(config.getDivision(), "CurrentStock,Item,ItemCode,ItemDescription", String.format("Warehouse eq guid'" + warehouseId + "'"));
        for (i = 0; i < itemDesc.get("d").getAsJsonObject().get("results").getAsJsonArray().size(); i++) {
            itemName = itemDesc.get("d").getAsJsonObject().get("results").getAsJsonArray().get(i).getAsJsonObject().get("ItemDescription").getAsString();
            itemID = itemDesc.get("d").getAsJsonObject().get("results").getAsJsonArray().get(i).getAsJsonObject().get("ItemCode").getAsString();
            itemStock = itemDesc.get("d").getAsJsonObject().get("results").getAsJsonArray().get(i).getAsJsonObject().get("CurrentStock").getAsInt();
            salesItemPrice = itemsApi.getItemPrice(config.getDivision(), "Price", String.format("ItemCode eq '" + itemID + "'"));
            itemPrice = salesItemPrice.get("d").getAsJsonObject().get("results").getAsJsonArray().get(0).getAsJsonObject().get("Price").getAsDouble();
            itemDetails=new JsonObject();
            itemDetails.addProperty("ID", itemID);
            itemDetails.addProperty("Name", itemName);
            itemDetails.addProperty("Stock", itemStock);
            itemDetails.addProperty("Price", itemPrice);
            Items.add(itemDetails);
        }
        jsonObject = new JsonObject();
        jsonObject.add("Items", Items);
        emitData(jsonObject);
    }


    private String refreshToken() {

        AccessApi accessApi= requestHelper.getAccessBuilder().create(AccessApi.class);

        Map<String, String> map= new HashMap<String, String>();

        map.put("client_id",clientId);
        map.put("client_secret",clientSecret);

        JsonObject res= accessApi.refreshToken(
                config.getOauth().getRefreshToken(),
                "refresh_token",
                map
        );

        return res.get("access_token").getAsString();
    }
}
