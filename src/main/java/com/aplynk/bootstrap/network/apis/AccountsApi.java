package com.aplynk.bootstrap.network.apis;

import com.google.gson.JsonObject;
import retrofit.http.*;

/**
 * Created by Anantaram on 17-03-2017.
 */
public interface AccountsApi {
    @GET("/v1/{division}/crm/Accounts")
    JsonObject getAccounts(@Path("division") String division, @Query("$select") String select, @Query("$filter") String filter);
    @GET("/v1/{division}/crm/Accounts")
    JsonObject getAccountsById(@Path("division") String division,@Query("$filter") String filter);
    @POST("/v1/{division}/crm/Accounts")
    JsonObject CreateAccount(@Path("division") String div, @Body JsonObject body);
    @POST("/v1/{division}/financial/GLAccounts")
    JsonObject CreateGLAccount(@Path("division") String div, @Body JsonObject body);

}
