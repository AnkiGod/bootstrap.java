package com.aplynk.bootstrap.network.apis;

import com.google.gson.JsonObject;
import retrofit.http.*;

/**
 * Created by Anantaram on 17-03-2017.
 */
public interface ItemsApi {

    @GET("/v1/{division}/inventory/ItemWarehouses")
    JsonObject getItemByWarehouseID(@Path("division") String div, @Query("$select") String select, @Query("$filter")String filter);

    @GET("/v1/{division}/logistics/SalesItemPrices")
    JsonObject getItemPrice(@Path("division") String div, @Query("$select") String select, @Query("$filter")String filter);

}
