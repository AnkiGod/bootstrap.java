package com.aplynk.bootstrap.network.apis;

import com.google.gson.JsonObject;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

import java.util.Map;

/**
 * Created by Anantaram on 17-03-2017.
 */
public interface AccessApi {
    @FormUrlEncoded
    @POST("/oauth2/token")
    JsonObject refreshToken(
            @Field("refresh_token") String refreshToken,
            @Field("grant_type") String grantType,
            @FieldMap Map<String,String> fields
    );
}
