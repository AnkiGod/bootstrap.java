package com.aplynk.bootstrap.network.apis;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import retrofit.http.*;

/**
 * Created by Anantaram on 06-04-2017.
 */
public interface WarehouseApi {
   @GET("/v1/{division}/inventory/Warehouses")
   JsonObject getWarehouseByCode(@Path("division") String division,@Query("$filter") String filter);
}
