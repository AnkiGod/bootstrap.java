package com.aplynk.bootstrap.network;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by soumya on 19-04-2016.
 */
public class RequestHelper {
    //TODO: Change the base url when implementing
    private static final String BASE_URL = "https://start.exactonline.nl/api";

    public RestAdapter getAccessBuilder() {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(BASE_URL);
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
            }
        });

        return builder.build();
    }

    public RestAdapter getNewAccessBuilder() {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(BASE_URL);
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
            }
        });

        return builder.build();
    }

    public RestAdapter getApiBuilder(final String token) {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(BASE_URL);
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
                request.addHeader("Content-Type", "application/json");
                request.addHeader("Authorization", "Bearer " + token);
            }
        });

        return builder.build();
    }
}
