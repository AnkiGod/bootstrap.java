/*package com.aplynk.bootstrap.actions;

import com.aplynk.bootstrap.blueprints.BaseComponent;
import com.aplynk.bootstrap.models.Config;
import com.aplynk.bootstrap.network.RequestHelper;
import com.aplynk.bootstrap.network.apis.AccessApi;
import com.aplynk.bootstrap.network.apis.AccountsApi;
import com.aplynk.bootstrap.network.apis.ItemsApi;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.elastic.api.EventEmitter;
import io.elastic.api.ExecutionParameters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.UUID.fromString;

/**
 * Created by Anantaram on 17-03-2017.

public class PostSalesOrder extends BaseComponent {

    public PostSalesOrder(EventEmitter eventEmitter) {
        super(eventEmitter);
    }

    private String ID;
    private UUID guid;
    private String clientId;
    private String clientSecret;
    private Config config;
    private JsonObject input;
    private JsonArray Items;
    private RequestHelper requestHelper;
    private ItemsApi itemsApi;

    @Override
    public void execute(ExecutionParameters parameters) {
        log("Input :: " + parameters.getMessage().getBody());
        log("Config :: " + parameters.getConfiguration());
        log("Snapshot :: " + parameters.getSnapshot());

        clientId=getEnv("client_id");
        clientSecret=getEnv("client_secret");
        config = new Gson().fromJson(parameters.getConfiguration(), Config.class);
        input=parameters.getMessage().getBody();
        Items= new JsonArray();

        log("Input Model : " + input);
        log("config Model : " + config);
        log("Client Id : " + clientId);
        log("Client Secret : " + clientSecret);


        for(int i=0;i<input.getAsJsonArray("lineItems").size();i++) {
            JsonObject myData=input.getAsJsonArray("lineItems").get(i).getAsJsonObject();
            Gson gson=new Gson();
            JsonElement element=gson.fromJson(myData.toString(),JsonElement.class);
            Items.add(element);
        }
        requestHelper=new RequestHelper();
        String token=refreshToken();
        CreateItem(token);
        CreateAccount(token);
    }

    private void CreateItem(String token) {
        itemsApi= requestHelper.getApiBuilder(token).create(ItemsApi.class);
        JsonObject itemresp = itemsApi.getItem(config.getDivision());
        JsonObject body = new JsonObject();
        int j;
        for (int i = 0; i < Items.size(); i++) {
            for (j = 0; j < itemresp.get("d").getAsJsonObject().get("results").getAsJsonArray().size(); j++) {
                if (Items.get(i).getAsJsonObject().get("product").getAsJsonObject().get("id").getAsString().equals(itemresp.get("d").getAsJsonObject().get("results").getAsJsonArray().get(j).getAsJsonObject().get("Code").getAsString())) {
                    break;
                }
            }
            if (j == itemresp.get("d").getAsJsonObject().get("results").getAsJsonArray().size()) {
                body.addProperty("Code", Items.get(i).getAsJsonObject().get("product").getAsJsonObject().get("id").getAsString());
                body.addProperty("Description", Items.get(i).getAsJsonObject().get("product").getAsJsonObject().get("name").getAsString());
                itemsApi.CreateItem(config.getDivision(),body);
            }
        }
    }

    private void CreateAccount(String token) {
        AccountsApi api= requestHelper.getApiBuilder(token).create(AccountsApi.class);
        ID=input.get("account").getAsJsonObject().get("id").getAsString();
        log(ID);
        log(UUID.fromString(ID).toString());
        guid=fromString(ID);
        JsonObject res=api.getAccountsById(config.getDivision(),String.format("ID eq guid'%s'",guid));
        if(res.get("d").getAsJsonObject().get("results").getAsJsonArray().size()==0){
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("Status","C");

            String AccountName=input.get("account").getAsJsonObject().get("name").getAsString();
            if(AccountName!=null)
            jsonObject.addProperty("Name", AccountName);

            log(guid.toString());
            jsonObject.addProperty("ID",guid.toString());


            String AddressLine1=input.get("billing_address").getAsJsonObject().get("street_1").getAsString();
            if(AddressLine1!=null)
                jsonObject.addProperty("AddressLine1", AddressLine1);

            String AddressLine2=input.get("billing_address").getAsJsonObject().get("street_2").getAsString();
            if(AddressLine2!=null)
                jsonObject.addProperty("AddressLine2", AddressLine2);

            String City=input.get("billing_address").getAsJsonObject().get("city").getAsString();
            if(City!=null)
                jsonObject.addProperty("City", City);

            String Country=input.get("billing_address").getAsJsonObject().get("country").getAsString();
            if(Country!=null)
                jsonObject.addProperty("Country", Country);

            String Email=input.get("account").getAsJsonObject().get("email").getAsString();
            if(Email!=null)
                jsonObject.addProperty("Email", Email);

            String Phone=input.get("billing_address").getAsJsonObject().get("phone").getAsString();
            if(Phone!=null)
                jsonObject.addProperty("Phone", Phone);

            String PostCode=input.get("billing_address").getAsJsonObject().get("postcode").getAsString();
            if(PostCode!=null)
                jsonObject.addProperty("PostCode", PostCode);

            api.CreateAccount(config.getDivision(), jsonObject);
        }
    }



    private String refreshToken() {
        AccessApi accessApi= requestHelper.getAccessBuilder().create(AccessApi.class);

        Map<String, String> map= new HashMap<String, String>();

        map.put("client_id",clientId);
        map.put("client_secret",clientSecret);

        JsonObject res= accessApi.refreshToken(
                config.getOauth().getRefreshToken(),
                "refresh_token",
                map
        );

        return res.get("access_token").getAsString();
    }
}
*/